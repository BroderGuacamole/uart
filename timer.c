#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "emp_type.h"
#include "systick.h"
#include "var.h"

 extern int ticks;
 INT8U ms   = 0;
 INT8U s    = 0;
 INT8U min  = 0;
 INT8U hour = 0;

void timer_var()
{
    if(ticks == 1)
    {
      ms++;
      if(ms==999)
      {
          ms = 0;
          s++;
          if(s == 59)
              {
              s= 0;
              min++;
              if(min == 59)
                  {
                  min = 0;
                  hour++;
                  }
              }
      }
    ticks--;
    }

}
