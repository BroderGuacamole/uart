#include <stdint.h>
#include "emp_type.h"
#include "tm4c123gh6pm.h"
#include "systick.h"  //changed from 5ms to 1ms
#include "timer.h"
#include "var.h"
#include "uart.h"


// line 10-90 is functions so the correct value for databits, parity and stopbits is given
INT32U lcrh_databits( INT8U antal_databits )
{
  if(( antal_databits < 5 ) || ( antal_databits > 8 ))
    antal_databits = 8;
  return(( (INT32U)antal_databits - 5 ) << 5 );  // Control bit 5-6, WLEN
}
/*****************************************************************************
*   Input    :
*   Output   :
*   Function : sets bit 5 and 6 according to the wanted number of data bits.
*               5: bit5 = 0, bit6 = 0.
*               6: bit5 = 1, bit6 = 0.
*               7: bit5 = 0, bit6 = 1.
*               8: bit5 = 1, bit6 = 1  (default).
*              all other bits are returned = 0
******************************************************************************/

INT32U lcrh_stopbits( INT8U antal_stopbits )
/*****************************************************************************
*   Input    :
*   Output   :
*   Function : sets bit 3 according to the wanted number of stop bits.
*               1 stpobit:  bit3 = 0 (default).
*               2 stopbits: bit3 = 1.
*              all other bits are returned = 0
******************************************************************************/
{
  if( antal_stopbits == 2 )
    return( 0x00000008 );       // return bit 3 = 1
  else
    return( 0x00000000 );       // return all zeros
}

INT32U lcrh_parity( INT8U parity )
/*****************************************************************************
*   Input    :
*   Output   :
*   Function : sets bit 1, 2 and 7 to the wanted parity.
*               'e':  00000110b.
*               'o':  00000010b.
*               '0':  10000110b.
*               '1':  10000010b.
*               'n':  00000000b.
*              all other bits are returned = 0
******************************************************************************/
{
  INT32U result;

  switch( parity )
  {
    case 'e':
      result = 0x00000006;
      break;
    case 'o':
      result = 0x00000002;
      break;
    case '0':
      result = 0x00000086;
      break;
    case '1':
      result = 0x00000082;
      break;
    case 'n':
    default:
      result = 0x00000000;
  }
  return( result );
}

void uart0_fifos_enable()
{

}
/*****************************************************************************
*   Input    :
*   Output   :
*   Function : Enable the tx and rx fifos
******************************************************************************/

void uart_setup(INT32U baud_rate, INT8U databits, INT8U stopbits, INT8U parity)
{
    INT32U BRD_DIV; //variable used to calculate the baudrate



    SYSCTL_RCGCUART_R |= 0x20;             // Enable clock for UART 0
    SYSCTL_RCGCGPIO_R |= 0x10;

    //used to setup GPIO pin 0 and 1 as TX and RX for UART0
    GPIO_PORTE_DEN_R   = 0x30; /* Make PA0 and PA1 as digital */
    GPIO_PORTE_AFSEL_R = 0x30;/* Use PA0,PA1 alternate function */
    GPIO_PORTE_PCTL_R  = 0x00110000; // PA0 and PA1 configure for UART module
    GPIO_PORTE_AMSEL_R = 0;    /* Turn off analg function*/



    //used to setup the desired Baud Rate
    BRD_DIV = 80000000 / baud_rate;    // CPU_clock/baud_rate = baudrate divsion
    UART5_IBRD_R = BRD_DIV / 64;
    UART5_FBRD_R = BRD_DIV & 0x0000003F;

    UART5_LCRH_R  |= 0x00000010; //Fifo diable

    UART5_CTL_R = 0x301; /* enable UART0, TXE, RXE */


    //used to setup the UARTLCRH register
    UART5_LCRH_R  = lcrh_databits( databits );
    UART5_LCRH_R += lcrh_stopbits( stopbits );
    UART5_LCRH_R += lcrh_parity( parity );

}

void UART5_Transmitter(INT32U data)
{
    while((UART5_FR_R & 0x20) != 0)/* wait until Tx buffer not full */
    {}
    UART5_DR_R = data;                  /* before giving it another byte */
}




