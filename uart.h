/*****************************************************************************
* University of Southern Denmark
* Embedded Programming (EMP)
*
* MODULENAME.: uart.h
*
* PROJECT....: EMP
*
* DESCRIPTION: Initialize the systick interrupt.
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 150215  MoH    Module created.
*
*****************************************************************************/


/***************************** Include files *******************************/
#include "emp_type.h"
/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/
void uart_setup(INT32U baud_rate, INT8U databits, INT8U stopbits, INT8U parity);
/*****************************************************************************
*   Input    : - Enter the wished baudrate here, the amount of databits, here it should be enter if there is used 1 or 2 stop bits, here paritybits is selctede
*   Output   : - Used to setup the UART0 port
*   Function : - Used to setup the UART0
******************************************************************************/


void UART5_Transmitter(INT32U data);
/*****************************************************************************
*   Input    : - Data that should be send
*   Output   : - Sends data to UART
*   Function : - Used to send data to the UART0
******************************************************************************/
